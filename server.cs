// Slap Command! - By Nicholaus04(48846)
// Inspired by Oxy's(260031) /slap command.

// Mostly made for Rose The Floran(25412).
// To be included in Server_BugCmd. (My own command add-on! (To be done/released...))

// USAGE:
// The pref variable below this comment wall controls the RNG bias for the slap velocity (Default is 200, like Oxy's)
// Basically, the velocity rolls a number between that number, and the inverted version of said number.
// Said random number is stored for the X velocity, same goes for Y and Z.
// Also, it doesn't have to be a positive number for the variable. (Nor does it have to be an int. It can also be a float too!)

// Now the command usage! (Must be admin/super admin/host to use these commands.):
// /slap <NAME> - Forces the named player to be sent flying into the air! 
// /slapblid <BL_ID> - Ditto, but uses the victim's BL_ID as input instead of a name.
// /slapall - Ditto (again...), but affects everybody on the server! (Super Admin Only!)

// REQUIRED ADD-ONS:
// Item_Skis - Needed for tumble(); This script will load the add-on itself if the function is not found.

// BEGIN CODE... NOW!

// Add-On Check:
if(!isFunction("tumble")){
  if(isFunction("LoadRequiredAddon")){%addon_rc=LoadRequiredAddOn("Item_Skis");if(%addon_rc!=$Error::None)return;}
  else return;
}

// Utility Functions:
function clrstr(%c,%str){
  %color[0]="\c0";
  %color[1]="\c1";
  %color[2]="\c2";
  %color[3]="\c3";
  %color[4]="\c4";
  %color[5]="\c5";
  %color[6]="\c6";
  %color[7]="\c7";
  %color[8]="\c8";
  %c=mClamp(%c,0,8);
  return %color[%c]@%str;
}

function isClassname(%obj,%cn){
	if($obj<0||!isObject(%obj))return false;
  return(strlwr(%cn)$=strlwr(%obj.getClassName()))?true:false;
}

function admincheck(%cl,%t){
  if(!isClassName(%cl,"GameConnection"))return false;
  %av[0]=%cl.isAdmin;%av[1]=%cl.isSuperAdmin;%av[2]=%cl.isHost;
  return(%av[mClamp(%t,0,2)])?true:false;
}

function clmsg(%cl,%msg){if(isClassName(%cl,"GameConnection")){%str=rtrim(%msg);if(strlen(%str))messageClient(%cl,'',%str);}}
function svmsg(%msg){%msg=rtrim(%msg);if(strlen(%msg))messageAll('',%msg);}

// Preferences:
$Pref::Server::SlapBias=200;
$Pref::Server::SlapEnabled=true;

// Strings:
$slapdone="has slapped";
$slapdoneself="has slapped themself!";
$slapdoneall="has slapped everyone!";
$usage="USAGE: /slap(or /slapblid) [Name or BL_ID]";
$blid_unknown="There's no such player with that BL_ID";
$name_unknown="There's no such player with that name";

// Main Code:
function tumbvel(%p,%v){
  if(!isClassName(%p,"Player"))return false;
  %p.setVelocity(%v);tumble(%p,0);
  return true;
}
function biasvel(){
  %bv=$Pref::Server::SlapBias;
  return getRandom(%bv,-%bv) SPC getRandom(%bv,-%bv) SPC getRandom(%bv,-%bv);
}

// Commands:
function serverCmdSlap(%cl,%n1,%n2,%n3,%n4,%n5,%n6,%n7,%n8,%n9,%n10,%n11,%n12,%n13,%n14,%n15,%n16,%n17,%n18,%n19){
  if(!$Pref::Server::SlapEnabled)return;
  if(!adminCheck(%cl,0))return;

  %name=
    %n1 SPC
    %n2 SPC
    %n3 SPC
    %n4 SPC
    %n5 SPC
    %n6 SPC
    %n7 SPC
    %n8 SPC
    %n9 SPC
    %n10 SPC
    %n11 SPC
    %n12 SPC
    %n13 SPC
    %n14 SPC
    %n15 SPC
    %n16 SPC
    %n17 SPC
    %n18 SPC
    %n19;
	%name=rtrim(%name);
  if(!strlen(%name)){clmsg(%cl,clrstr(6,$usage));return;}

  %vcl=findClientByName(%name);
	if(!isObject(%vcl)){clmsg(%cl,clrstr(0,$name_unknwon));return;}
  %vpo=%vcl.player; 
  %cname=%cl.getPlayerName();
  %vname=%vcl.getPlayerName();
  
  %bv=biasvel();
  if(tumbvel(%vpo,%bv)){
    %same=(%vcl==%cl)?true:false;
    if(!%same)%output=clrstr(3,%cname) SPC clrstr(6,$slapdone) SPC clrstr(3,%vname) @ clrstr(6,"!");
    else %output=clrstr(3,%cname) SPC clrstr(6,$slapdoneself);
    svmsg(%output);
    return;
  }
}
function serverCmdSlapBLID(%cl,%blid){
  if(!$Pref::Server::SlapEnabled)return;
  if(!adminCheck(%cl,0))return;

	if(%blid$=""){clmsg(%cl,clrstr(6,$usage));return;}

	%vcl=findClientByBL_ID(%blid);
	if(!isClassName(%vcl,"GameConnection")){clmsg(%cl,clrstr(0,$blid_unknown));return;}
  %vpo=%vcl.player;
  %cname=%cl.getPlayerName();
  %vname=%vcl.getPlayerName();

  %bv=biasvel();
  if(tumbvel(%vpo,%bv)){
    %same=(%vcl==%cl)?true:false;
    if(!%same)%output=clrstr(3,%cname) SPC clrstr(6,$slapdone) SPC clrstr(3,%vname) @ clrstr(6,"!");
    else %output=clrstr(3,%cname) SPC clrstr(6,$slapdoneself);
    svmsg(%output);
    return;
  }
}
function serverCmdSlapAll(%cl){
  if(!$Pref::Server::SlapEnabled)return;
  if(!adminCheck(%cl,1))return;

	%count=clientGroup.getCount();
	for(%i=0;%i<%count;%i++){
		%cl=clientGroup.getObject(%i);
		if(isClassName(%cl,"GameConnection")){%bv=biasvel();tumbvel(%cl.player,%bv);}
	}
	svmsg(clrstr(3,%cl.getPlayerName()) SPC clrstr(6,$slapdoneall));
}
